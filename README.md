**Installation notes**

- Checkout project.
- In terminal go to `persistence-serivce` directory
- Run `sbt docker:publishLocal` command
- Go to `signup-service` directory
- Run `sbt docker:publishLocal` command
- To verify that docker images are published run `docker images` command. 
You should see something like:
```
REPOSITORY               TAG                 IMAGE ID            CREATED             SIZE
signup-service           1.0                 5c9e6ea18c4b        16 seconds ago      818MB
persistence-service      1.0                 79bce1647f84        7 minutes ago       842MB
```
- After images are published, go to `docker/data` directory and execute `docker-compose up -d`.
- Check that kafka is up and running.<br>
By default kafka creates `player` topic with a single partition and replica.<br>
But sometimes it hangs on `creating topics: player:1:1` step.<br>
Probably because kafka container starts before zookeeper.<br>
Check that by running `docker ps` to find out kafka container id.<br>
Then run `docker logs -f ${container_id}`.<br>
If it got stuck, run `docker-compose down`.<br> 
And then `docker-compose up -d` again.<br>
- Go to `functional-test` directory
- Run `sbt test`

