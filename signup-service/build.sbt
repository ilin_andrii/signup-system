name := "signup-service"
 
version := "1.0" 
      
lazy val `signup-service` = (project in file(".")).enablePlugins(PlayScala)

resolvers += "scalaz-bintray" at "https://dl.bintray.com/scalaz/releases"
resolvers += "Akka Snapshot Repository" at "http://repo.akka.io/snapshots/"
resolvers += Resolver.bintrayRepo("cakesolutions", "maven")

scalaVersion := "2.12.3"

libraryDependencies += guice
libraryDependencies += "net.cakesolutions" %% "scala-kafka-client" % "0.11.0.0"
libraryDependencies += "org.scalatestplus.play" %% "scalatestplus-play" % "3.1.+" % "test"
libraryDependencies += "org.mockito" % "mockito-core" % "2.11.0" % "test"

PlayKeys.devSettings := Seq("play.server.http.port" -> "9002", "kafka.host" -> "localhost")
