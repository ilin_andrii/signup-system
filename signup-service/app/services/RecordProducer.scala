package services

import javax.inject.Singleton

import cakesolutions.kafka.{KafkaProducer, KafkaProducerRecord}
import com.google.inject.Inject
import org.apache.kafka.clients.producer.RecordMetadata
import org.apache.kafka.common.serialization.StringSerializer
import play.api.Configuration
import play.api.libs.json.{Json, Writes}

import scala.concurrent.Future

@Singleton
class RecordProducer @Inject()(configuration: Configuration) {

  private val ProducerConf = KafkaProducer.Conf(
    keySerializer = new StringSerializer, // won't be used
    valueSerializer = new StringSerializer,
    bootstrapServers = configuration
      .getOptional[String]("kafka.host")
      .getOrElse("localhost") + ":9092"
  )

  private[services] val producer = KafkaProducer(ProducerConf)

  def send[V](record: V, topic: String)(implicit recordWrites: Writes[V]): Future[RecordMetadata] = {
    val jsonStringRecord = Json.toJson(record).toString
    val producerRecord = KafkaProducerRecord[String, String](topic, jsonStringRecord)
    producer.send(producerRecord)
  }
}
