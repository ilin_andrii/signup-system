package services

import java.util.UUID
import javax.inject.{Inject, Singleton}

import model.{DtoPlayer, KafkaPlayer}
import play.api.{Configuration, Logger}
import services.PlayerProducer.logger

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class PlayerProducer @Inject()(recordProducer: RecordProducer,
                               configuration: Configuration)(implicit ec: ExecutionContext) {

  def send(player: DtoPlayer): Future[UUID] = {
    val topic = configuration.getOptional[String]("player.topic").getOrElse("default")
    val uuid = generateUUID
    val kafkaPlayer = KafkaPlayer(uuid, player.email, player.password)

    logger.debug(s"Sending player record: [$player]")

    recordProducer
      .send(kafkaPlayer, topic)
      .map(_ => uuid)
  }

  private[services] def generateUUID: UUID = UUID.randomUUID //for testing purposes
}

object PlayerProducer {
  private val logger = Logger(classOf[PlayerProducer])
}
