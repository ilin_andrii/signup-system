package controllers

import javax.inject.{Inject, Singleton}

import model.DtoPlayer
import play.api.mvc.{AbstractController, ControllerComponents, Request}
import services.PlayerProducer

import scala.concurrent.ExecutionContext
import scala.util.control.NonFatal

@Singleton
class PlayerController @Inject()(cc: ControllerComponents,
                                 playerProducer: PlayerProducer)(implicit ec: ExecutionContext) extends AbstractController(cc) {

  def create = Action.async(parse.json[DtoPlayer]) { request: Request[DtoPlayer] =>
    val player = request.body
    playerProducer.send(player)
      .map(uuid => Ok(uuid.toString))
      .recover {
        case NonFatal(e) => InternalServerError(e.getMessage)
      }
  }
}
