package model

import java.util.UUID

import play.api.libs.json.{Format, Json}

case class DtoPlayer(email: String, password: String)

object DtoPlayer {
  implicit val dtoPlayerFormat: Format[DtoPlayer] = Json.format[DtoPlayer]
}

case class KafkaPlayer(uuid: UUID, email: String, password: String)

object KafkaPlayer {
  implicit val kafkaPlayerFormat: Format[KafkaPlayer] = Json.format[KafkaPlayer]
}
