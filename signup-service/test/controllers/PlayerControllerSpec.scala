package controllers

import java.util.UUID

import model.DtoPlayer
import org.mockito.Mockito._
import org.scalatest.mockito.MockitoSugar
import org.scalatestplus.play.PlaySpec
import play.api.libs.json.Json
import play.api.mvc._
import play.api.test.FakeRequest
import play.api.test.Helpers._
import services.PlayerProducer

import scala.concurrent.{ExecutionContext, Future}

class PlayerControllerSpec extends PlaySpec with Results with MockitoSugar {

  private trait TestContext {
    val player = DtoPlayer("test@test.com", "test")
    val uuid = UUID.randomUUID
    val request = Request(FakeRequest(), player)

    val playerProducer = mock[PlayerProducer]
    val controllerComponents = stubControllerComponents()

    val playerController = new PlayerController(controllerComponents, playerProducer)(ExecutionContext.global)
  }

  "PlayerController" should {
    "return player's UUID" when {
      "player was created successfully" in new TestContext {
        val uuidFuture = Future.successful(uuid)

        when(playerProducer.send(player)).thenReturn(uuidFuture)

        val result = playerController.create.apply(request)
        contentAsString(result) mustBe uuid.toString
      }
    }

    "result in internal server error" when {
      "player creation ended with an error" in new TestContext {
        val exceptionMessage = "Exception during player creation"
        val uuidFuture: Future[UUID] = Future.failed(new RuntimeException(exceptionMessage))

        when(playerProducer.send(player)).thenReturn(uuidFuture)

        val result = playerController.create.apply(request)
        status(result) mustBe 500
        contentAsString(result) mustBe exceptionMessage
      }
    }
  }
}
