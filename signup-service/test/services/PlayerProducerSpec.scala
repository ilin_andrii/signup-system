package services

import java.util.UUID

import model.{DtoPlayer, KafkaPlayer}
import org.apache.kafka.clients.producer.RecordMetadata
import org.mockito.ArgumentMatchers.{any, eq => equiv}
import org.mockito.Mockito._
import org.scalatest.mockito.MockitoSugar
import org.scalatest.{AsyncWordSpec, Matchers}
import play.api.Configuration
import play.api.libs.json.Writes

import scala.concurrent.Future

class PlayerProducerSpec extends AsyncWordSpec with MockitoSugar with Matchers {

  "PlayerProducer" should {
    "return player uuid" when {
      "player was sent to message queue successfully" in {
        val recordProducer = mock[RecordProducer]
        val configuration = mock[Configuration]

        val player = DtoPlayer("test@test.com", "test")
        val topic = "testTopic"
        val futureRecordMetadata = Future.successful[RecordMetadata](None.orNull)
        val generatedUUID = UUID.randomUUID

        val playerProducer = new PlayerProducer(recordProducer, configuration) {
          override private[services] def generateUUID = generatedUUID
        }

        when(configuration.getOptional[String]("player.topic")).thenReturn(Option(topic))
        when(recordProducer.send(any[KafkaPlayer], equiv(topic))(any[Writes[KafkaPlayer]])).thenReturn(futureRecordMetadata)

        playerProducer.send(player).map { uuid =>
          uuid shouldBe generatedUUID
        }
      }
    }
  }
}
