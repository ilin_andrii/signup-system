package services

import java.util.UUID

import cakesolutions.kafka.KafkaProducer
import model.KafkaPlayer
import org.apache.kafka.clients.producer.MockProducer
import org.apache.kafka.common.serialization.StringSerializer
import org.mockito.Mockito.when
import org.scalatest.mockito.MockitoSugar
import org.scalatest.{AsyncWordSpec, Matchers}
import play.api.Configuration

class RecordProducerSpec extends AsyncWordSpec with Matchers with MockitoSugar {

  "RecordProducer" should {
    "send message to topic" in {
      val mockProducer = new MockProducer(true, new StringSerializer, new StringSerializer)
      val kafkaProducerStub = new KafkaProducer[String, String](mockProducer)
      val kafkaPlayer = KafkaPlayer(UUID.randomUUID, "test@test.com", "test")

      val configuration = mock[Configuration]
      when(configuration.getOptional[String]("kafka.host")).thenReturn(None)

      val recordProducer = new RecordProducer(configuration) {
        override private[services] val producer = kafkaProducerStub
      }

      recordProducer
        .send(kafkaPlayer, "testTopic")
        .map { metadata =>
          metadata.topic shouldBe "testTopic"
        }
    }
  }
}
