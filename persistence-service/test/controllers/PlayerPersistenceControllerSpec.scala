package controllers

import java.util.UUID

import model.Player
import org.mockito.Mockito._
import org.scalatest.mockito.MockitoSugar
import org.scalatestplus.play.PlaySpec
import play.api.libs.json.Json
import play.api.test.FakeRequest
import play.api.test.Helpers.{status, _}
import services.PlayerRepository

import scala.concurrent.{ExecutionContext, Future}

class PlayerPersistenceControllerSpec extends PlaySpec with MockitoSugar {

  private trait TestContext {
    val controllerComponents = stubControllerComponents()
    val playerRepository = mock[PlayerRepository]
    val uuid = UUID.randomUUID
    val player = Player(uuid, "test@test.com", "test")
    val playerJson = Json.toJson(player)
  }

  "PlayerPersistenceController" should {
    "return player by UUID" in new TestContext {
      val playerFuture = Future.successful(Option(player))

      val playerPersistenceController = new PlayerPersistenceController(controllerComponents, playerRepository)(ExecutionContext.global)

      when(playerRepository.queryByUUID(uuid)).thenReturn(playerFuture)

      val result = playerPersistenceController.queryByUUID(uuid).apply(FakeRequest(method = "GET", path = s"players/$uuid"))

      status(result) mustBe 200
      contentAsJson(result) mustBe playerJson
    }

    "result in not found error" when {
      "player is not found by given UUID" in new TestContext {
        val playerFuture = Future.successful(None)

        val playerPersistenceController = new PlayerPersistenceController(controllerComponents, playerRepository)(ExecutionContext.global)

        when(playerRepository.queryByUUID(uuid)).thenReturn(playerFuture)

        val result = playerPersistenceController.queryByUUID(uuid).apply(FakeRequest(method = "GET", path = s"players/$uuid"))

        status(result) mustBe 404
      }
    }
  }
}
