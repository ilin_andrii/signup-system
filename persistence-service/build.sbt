name := "persistence-service"

version := "1.0"

lazy val `persistence-service` = (project in file(".")).enablePlugins(PlayScala)

resolvers += "scalaz-bintray" at "https://dl.bintray.com/scalaz/releases"
resolvers += "Akka Snapshot Repository" at "http://repo.akka.io/snapshots/"
resolvers += Resolver.bintrayRepo("cakesolutions", "maven")

scalaVersion := "2.12.2"

libraryDependencies += guice
libraryDependencies += "net.cakesolutions" %% "scala-kafka-client" % "0.11.0.0"
libraryDependencies += "org.reactivemongo" %% "play2-reactivemongo" % "0.12.6-play26"
libraryDependencies += "org.scalatestplus.play" %% "scalatestplus-play" % "3.1.+" % "test"
libraryDependencies += "org.mockito" % "mockito-core" % "2.11.0" % "test"
libraryDependencies += "com.typesafe.akka" % "akka-testkit_2.12" % "2.5.4" % "test"


PlayKeys.devSettings := Seq(
  "play.server.http.port" -> "9001",
  "kafka.host" -> "localhost",
  "mongodb.uri" -> "mongodb://localhost:27017/players"
)
      