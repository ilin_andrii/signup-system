import com.google.inject.AbstractModule
import play.api.libs.concurrent.AkkaGuiceSupport
import services.PlayerConsumer

class Module extends AbstractModule with AkkaGuiceSupport {
  override def configure(): Unit = {
    bindActor[PlayerConsumer]("player-consumer")
  }
}
