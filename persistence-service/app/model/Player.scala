package model

import java.util.UUID

import play.api.libs.json.{Json, OFormat}

case class Player(uuid: UUID, email: String, password: String)

object Player {
  implicit val playerFormat: OFormat[Player] = Json.format[Player]
}
