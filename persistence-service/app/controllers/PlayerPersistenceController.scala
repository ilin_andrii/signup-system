package controllers

import java.util.UUID

import com.google.inject.Inject
import play.api.libs.json.Json
import play.api.mvc.{AbstractController, ControllerComponents}
import services.PlayerRepository

import scala.concurrent.ExecutionContext

class PlayerPersistenceController @Inject()(cc: ControllerComponents,
                                            playerRepository: PlayerRepository)(implicit ec: ExecutionContext) extends AbstractController(cc) {

  def health = Action(Ok)

  def queryByUUID(uuid: UUID) = Action.async {
    playerRepository.queryByUUID(uuid).map { mbPlayer =>
      mbPlayer
        .map(player => Ok(Json.toJson(player)))
        .getOrElse(NotFound)
    }
  }
}
