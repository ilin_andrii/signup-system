package services

import javax.inject.Inject

import akka.actor.{Actor, Timers}
import model.Player
import org.apache.kafka.clients.consumer.ConsumerRecord
import play.api.{Configuration, Logger}
import play.api.libs.json.Json
import services.PlayerConsumer._

import scala.concurrent.duration._

class PlayerConsumer @Inject()(playerRepository: PlayerRepository,
                               recordConsumer: RecordConsumer,
                               configuration: Configuration) extends Actor with Timers {
  private val topic = configuration
    .getOptional[String]("player.topic")
    .getOrElse("default")


  timers.startSingleTimer(TimerKey, Init, 0 seconds)

  override def receive = {
    case Init =>
      recordConsumer.subscribe(topic)
      timers.startPeriodicTimer(TimerKey, Consume, 500 milliseconds)
    case Consume => recordConsumer.poll.foreach(handleRecord)
  }

  private def handleRecord(record: ConsumerRecord[String, String]): Unit = {
    val playerString = record.value
    val player = Json.fromJson[Player](Json.parse(playerString)).get
    logger.debug(s"Received player data: [id: ${player.uuid}, email: ${player.email}, password: ${player.password}]")
    playerRepository.create(player)
  }
}

object PlayerConsumer {

  private val logger = Logger(classOf[PlayerConsumer])

  case object TimerKey

  case object Init

  case object Consume

}
