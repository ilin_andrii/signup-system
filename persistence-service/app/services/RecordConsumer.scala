package services

import javax.inject.{Inject, Singleton}

import cakesolutions.kafka.KafkaConsumer
import cakesolutions.kafka.KafkaConsumer.Conf
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.apache.kafka.common.serialization.StringDeserializer
import play.api.{Configuration, Logger}
import services.RecordConsumer.logger

import scala.collection.JavaConverters._

@Singleton
class RecordConsumer @Inject()(configuration: Configuration) {
  private val ConsumerConf = Conf(
    keyDeserializer = new StringDeserializer,
    valueDeserializer = new StringDeserializer,
    bootstrapServers = configuration
      .getOptional[String]("kafka.host")
      .getOrElse("localhost") + ":9092",
    groupId = "default")

  private[services] val consumer = KafkaConsumer(ConsumerConf)

  def subscribe(topics: String*): Unit = {
    consumer.subscribe(topics.asJava)
  }

  def poll: Iterable[ConsumerRecord[String, String]] = {
    consumer.poll(0).asScala
  }

  logger.debug("Record consumer created")
}

object RecordConsumer {
  private val logger = Logger(classOf[RecordConsumer])
}
