package services

import java.util.UUID
import javax.inject.{Inject, Singleton}

import model.Player
import model.Player._
import play.api.{Configuration, Logger}
import play.modules.reactivemongo.ReactiveMongoApi
import reactivemongo.api.commands.WriteResult
import reactivemongo.bson.BSONDocument
import reactivemongo.play.json._
import reactivemongo.play.json.collection.JSONCollection
import services.PlayerRepository.logWriteResult

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class PlayerRepository @Inject()(mongoApi: ReactiveMongoApi,
                                 configuration: Configuration)(implicit ec: ExecutionContext) {

  private val playerCollectionName = configuration
    .getOptional[String]("player.collection")
    .getOrElse("default")

  private def playerCollection: Future[JSONCollection] = mongoApi.database.map(_.collection(playerCollectionName))

  def create(player: Player): Unit = {
    playerCollection.flatMap(_.insert(player)).foreach(logWriteResult)
  }

  def queryByUUID(uuid: UUID): Future[Option[Player]] = {
    val query = BSONDocument("uuid" -> s"$uuid")
    playerCollection.flatMap(_.find(query).one[Player])
  }
}

object PlayerRepository {
  private val logger = Logger(classOf[PlayerRepository])

  private def logWriteResult(result: WriteResult): Unit = {
    if (result.ok) {
      logger.debug(s"Successfully written ${result.n} record(s)")
    } else {
      logger.debug(s"Following errors have occurred during database write ${result.writeErrors.mkString("[", ";", "]")}")
    }
  }
}
