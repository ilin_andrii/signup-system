
name := "functional-test"

version := "1.0"

lazy val `functional-test` = project in file(".")

libraryDependencies += "com.typesafe.play" %% "play-ahc-ws-standalone" % "1.1.2"
libraryDependencies += "com.typesafe.play" %% "play-ws-standalone-json" % "1.1.2"
libraryDependencies += "org.scalactic" %% "scalactic" % "3.0.4"
libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.4" % "test"