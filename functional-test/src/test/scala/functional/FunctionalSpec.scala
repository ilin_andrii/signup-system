package functional

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import org.scalatest.{AsyncWordSpec, Matchers}
import play.api.libs.json.{JsObject, JsString}
import play.api.libs.ws.ahc.StandaloneAhcWSClient
import play.api.libs.ws.{DefaultBodyReadables, DefaultBodyWritables, JsonBodyReadables, JsonBodyWritables}

import scala.concurrent.Future

class FunctionalSpec extends AsyncWordSpec with Matchers
  with JsonBodyWritables
  with JsonBodyReadables
  with DefaultBodyReadables
  with DefaultBodyWritables {

  "Distributed sign up system" should {
    "create a new player" which {
      "can then be queried by UUID" in {
        implicit val actorSystem: ActorSystem = ActorSystem()
        implicit val materializer: ActorMaterializer = ActorMaterializer()

        val playerJsonFields = Seq(
          "email" -> JsString("test@test.com"),
          "password" -> JsString("test")
        )

        val wsClient = StandaloneAhcWSClient()
        val body = JsObject(playerJsonFields)

        for {
          uuid <- wsClient.url("http://localhost:9002/players")
            .withHttpHeaders("Content-Type" -> "application/json")
            .post(body)
            .map(_.body[String])
          player <- poll(wsClient.url(s"http://localhost:9001/players/$uuid").get)(1000, response => response.body.nonEmpty)
            .map(_.body)
        } yield {
          val jsonFields = ("uuid" -> JsString(uuid.toString)) +: playerJsonFields
          val expected = JsObject(jsonFields).toString
          player shouldBe expected
        }
      }
    }
  }

  private def poll[T](action: => Future[T])(retries: Int, condition: T => Boolean): Future[T] = {
    if (retries > 0) {
      val future = action.filter(condition)
      future.recoverWith {
        case _ => poll(action)(retries - 1, condition)
      }
    } else {
      action
    }
  }
}
